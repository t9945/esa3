Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
}

function delay(n){
  return new Promise(function(resolve){
  setTimeout(resolve, n*1000);
});
}

function range(start, end) {
  var indexRange = [];
  for (let i = start; i <= end; i++) {
      indexRange.push(i);
  }

  return indexRange;
}

var highlightColor = "#2596be";
var correctColor = "#02b131";
var wrongColor = "#fd4242";

var visualTitle = document.getElementById("visual-title");
var stackTitle = document.getElementById("stack-title");

var expressionLabel = document.getElementById("expression-label");
var expressionInput = document.getElementById("expression-input");
var selectLengthMenu = document.getElementById("select-length");

const tutorialButton = document.querySelector('#tutorial-button');
const createButton = document.querySelector('#create-expression-button');
const wrongButton = document.querySelector('#create-wrong-expression-button');
const playButton = document.querySelector('#play-button');

//SVG Elements
var node0 = document.getElementById("nodeq0");
var arrow0To1 = document.getElementById("0to1");

var node1 = document.getElementById("nodeq1");
var arrow1to2 = document.getElementById("1to2");
var arrow1to3 = document.getElementById("1to3");

var node2 = document.getElementById("nodeq2");
var arrow2to2 = document.getElementById("2to2");
var arrow2to4 = document.getElementById("2to4");

var node3 = document.getElementById("nodeq3");
var arrow3to2 = document.getElementById("3to2");
var arrow3to5 = document.getElementById("3to5");

var node4 = document.getElementById("nodeq4");
var arrow4to4 = document.getElementById("4to4");
var arrow4to3 = document.getElementById("4to3");
var arrow4to6 = document.getElementById("4to6");

var node5 = document.getElementById("nodeq5");
var arrow5to3 = document.getElementById("5to3");
var arrow5to4 = document.getElementById("5to4");
var arrow5to6 = document.getElementById("5to6");

var node6 = document.getElementById("nodeq6");
//


var expressionLength = 5; // 3, 5, 15, 25

selectLengthMenu.onchange = function() {
  var value = selectLengthMenu.value;
  var valueInt = parseInt(value, 10);
  expressionLength = valueInt;
}

var expressionS = "";
var expressionToChange;

function createExpression() {
  expressionLabel.style.letterSpacing = "7px";

  var numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  var numbersWoZero = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  var operators = ["+", "-", "*", "/"];

  resetGraph();
  visualTitle.innerHTML = "Graph";
  stackTitle.innerHTML = "";

  var expressionString = "";

  var state = 0;
  var isRunning = false;

  isRunning = true;

  var possibleCases = [];
  var expCase = 0;

  while (isRunning) {

      switch (expressionLength) {
        case 3:
          possibleCases.push(3);
          possibleCases.push(33);
          expCase = possibleCases.random();
          possibleCases = [];
          break;

        case 5:
          possibleCases.push(5);
          possibleCases.push(55);
          expCase = possibleCases.random();
          possibleCases = [];
          break;

        case 15:
          possibleCases.push(15);
          possibleCases.push(1515);
          possibleCases.push(151515);
          expCase = possibleCases.random();
          possibleCases = [];
          break;

        case 25:
          possibleCases.push(25);
          possibleCases.push(2525);
          possibleCases.push(252525);
          expCase = possibleCases.random();
          possibleCases = [];
          break;
      }


    switch (expCase) {
      /************************************************************************/
      case 3:
        var number1 = numbers.random();
        var operator = operators.random();
        var number2 = numbersWoZero.random();

        expressionString += number1 + operator + number2;

        isRunning = false;
        break;

      case 33:
        var number = numbers.random();

        expressionString += "(" + number + ")";

        isRunning = false;
        break;

      /************************************************************************/
        case 5:
          var number1 = numbers.random();
          var operator1 = operators.random();
          var number2 = numbersWoZero.random();
          var operator2 = operators.random();
          var number3 = numbersWoZero.random();

          expressionString += number1 + operator1 + number2 + operator2 + number3;

          isRunning = false;
          break;

        case 55:
          var number1 = numbers.random();
          var operator1 = operators.random();
          var number2 = numbersWoZero.random();

          expressionString += "(" + number1 + operator1 + number2 + ")";

          isRunning = false;
          break;
      /************************************************************************/

      /************************************************************************/

      case 15:
        var number1 = numbers.random();
        var number2 = numbersWoZero.random();
        var number3 = numbersWoZero.random();
        var number4 = numbersWoZero.random();
        var number5 = numbersWoZero.random();
        var number6 = numbersWoZero.random();
        var number7 = numbersWoZero.random();
        var number8 = numbersWoZero.random();

        var operator1 = operators.random();
        var operator2 = operators.random();
        var operator3 = operators.random();
        var operator4 = operators.random();
        var operator5 = operators.random();
        var operator6 = operators.random();
        var operator7 = operators.random();

        expressionString += number1 + operator1 + number2 + operator2 + number3 + operator3 + number4 + operator4 + number5 + operator5 + number6 + operator6 + number7 + operator7 + number8;

        isRunning = false;
        break;

      case 1515:
        var number1 = numbers.random();
        var number2 = numbersWoZero.random();
        var number3 = numbersWoZero.random();
        var number4 = numbersWoZero.random();
        var number5 = numbersWoZero.random();
        var number6 = numbersWoZero.random();
        var number7 = numbersWoZero.random();
        var number8 = numbersWoZero.random();

        var operator1 = operators.random();
        var operator2 = operators.random();
        var operator3 = operators.random();
        var operator4 = operators.random();
        var operator5 = operators.random();
        var operator6 = operators.random();
        var operator7 = operators.random();

        expressionString += "(" + number1 + operator1 + number2 + operator2 + number3 + operator3 + number4 + operator4 + number5 + operator5 + number6 + operator6 + number7 + ")";

        isRunning = false;
        break;

      case 151515:
        var number1 = numbers.random();
        var number2 = numbersWoZero.random();
        var number3 = numbersWoZero.random();
        var number4 = numbersWoZero.random();
        var number5 = numbersWoZero.random();
        var number6 = numbersWoZero.random();

        var operator1 = operators.random();
        var operator2 = operators.random();
        var operator3 = operators.random();
        var operator4 = operators.random();
        var operator5 = operators.random();

        expressionString += "(" + number1 + operator1 + number2 + ")" + operator2 + number3 + operator3 + "(" + number4 + operator4 + number5 + operator5 + number6 + ")";

        isRunning = false;
        break;

      /************************************************************************/
    }

  }

  expressionLabel.innerHTML = expressionString;
  expressionS = expressionString;
  expressionToChange = expressionString;

  playButton.disabled = false;
}
createButton.addEventListener("click", createExpression);

function createWrongExpression() {
  createExpression();

  var numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  var operators = ["+", "-", "*", "/"];
  var brackets = ["(", ")"];

  resetGraph();
  visualTitle.innerHTML = "Graph";
  stackTitle.innerHTML = "";

  var count = expressionToChange.length;
  var index = range(0, count - 1);
  var randomIndex = index.random();

  var charToReplace = expressionToChange.charAt(randomIndex);

  if (numbers.includes(charToReplace)) {
    expressionToChange = expressionToChange.substring(0, randomIndex) + operators.random()
    + expressionToChange.substring(randomIndex + 1);

  } else if (operators.includes(charToReplace)) {
    expressionToChange = expressionToChange.substring(0, randomIndex) + numbers.random() + expressionToChange.substring(randomIndex + 1);

  } else if (brackets.includes(charToReplace)) {
    expressionToChange = expressionToChange.substring(0, randomIndex) + numbers.random() + expressionToChange.substring(randomIndex + 1);
  }

  //expressionToChange = expressionToChange.substring(0, randomIndex) + "H" + expressionToChange.substring(randomIndex + 1);
  expressionString = expressionToChange;
  expressionS = expressionToChange;
  expressionLabel.innerHTML = expressionString;

}
wrongButton.addEventListener("click", createWrongExpression);

expressionInput.onclick = function() {
  expressionInput.style.color = "#ffffff";
};

var isExpCorrect = true;

async function check(expression) {
  var numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  var operators = ["+", "-", "*", "/"];

  if (event.key === "Enter") {

  var isRunning = false;
  playButton.disabled = false;

  var exp = expression.value;

  var node = 0;
  var nextNode = 0;

  var firstChar = exp[0];
  var openBrackets = 0;

  var count = exp.length;
  var indexPath = 0;

  if (count === 0) {
    expressionInput.placeholder = "Expression is empty!";
    await delay(1.75);
    expressionInput.placeholder = "e.g. 1+2*3";
    playButton.disabled = true;
    return;
  }

  expressionS = exp;
  expressionLabel.innerHTML = exp;

  if (numbers.includes(firstChar)) {
    isRunning = true;
    node = 1;
    nextNode = 3;
    indexPath = 0;

  } else if (firstChar === "(") {
    isRunning = true;
    node = 1;
    nextNode = 2;
    indexPath += 1;
    openBrackets += 1;

  } else {
    expressionInput.style.color = wrongColor;
    isRunning = false;
    return;
  }

  while (isRunning) {

    switch (nextNode) {

      case 2:

        if (numbers.includes(exp[indexPath])) {
          indexPath += 1;
          node = 2;
          nextNode = 4;

        } else if (exp[indexPath] === "(") {
          indexPath += 1;
          node = 2;
          nextNode = 2;
          openBrackets += 1;

        } else {
          expressionInput.style.color = wrongColor;
          isRunning = false;
        }

        break;

      case 3:

      if (numbers.includes(exp[indexPath])) {
        indexPath += 1;
        node = 3;
        nextNode = 5;

      } else if (exp[indexPath] === "(") {
        indexPath += 1;
        node = 3;
        nextNode = 2;
        openBrackets += 1;

      } else {
        expressionInput.style.color = wrongColor;
        isRunning = false;
      }

        break;

      case 4:

        if (operators.includes(exp[indexPath])) {
          indexPath += 1;
          node = 4;
          nextNode = 3;

        } else if (exp[indexPath] === ")") {
          indexPath += 1;
          node = 4;
          nextNode = 4;
          openBrackets -= 1;

        } else if (indexPath === count) {
          if (openBrackets === 0) {
            node = 4;
            nextNode = 6;
          }
           else {
             expressionInput.style.color = wrongColor;
             isRunning = false;
          }
        }

        break;

      case 5:

        if (operators.includes(exp[indexPath])) {
          indexPath += 1;
          node = 5;
          nextNode = 3;

        } else if (exp[indexPath] === ")") {
          indexPath += 1;
          node = 5;
          nextNode = 4;
          openBrackets -= 1;

        } else if (numbers.includes(exp[indexPath])) {
          expressionInput.style.color = wrongColor;
          isRunning = false;

        } else if (exp[indexPath]) {
          expressionInput.style.color = wrongColor;
          isRunning = false;

        } else if (indexPath === count && numbers.includes(exp[count - 1])) {
          if (openBrackets === 0) {

          } else {
            expressionInput.style.color = wrongColor;
            isRunning = false;
          }

          node = 5;
          nextNode = 6;
        }

        break;

      case 6:
      expressionInput.style.color = correctColor;
        isRunning = false;
        break;
  }
}
}
}

async function play() {
  var numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  var operators = ["+", "-", "*", "/"];

    playButton.disabled = true;
    createButton.disabled = true;
    wrongButton.disabled = true;

    resetGraph();
    visualTitle.innerHTML = "Graph = ";
    stackTitle.innerHTML = "Stack: "

    var isRunning = false;

    var exp = expressionS;

    var node = 0;
    var nextNode = 0;

    var firstChar = exp[0];
    var openBrackets = 0;

    var count = exp.length;
    var indexPath = 0;

    var pathElements = [];

    var stackString = "";

    node0.style.fill = highlightColor;
    arrow0To1.style.fill = highlightColor;
    node1.style.fill = highlightColor;

    stackString += "$";
    stackTitle.innerHTML = "Stack: " + stackString;

    if (numbers.includes(firstChar)) {
      isRunning = true;
      await delay(1);
      resetGraph();
      arrow1to3.style.fill = highlightColor;
      node3.style.fill = highlightColor;
      node = 1;
      nextNode = 3;
      indexPath = 0;

      pathElements.push(arrow1to3);
      pathElements.push(node3);

    } else if (firstChar === "(") {
      isRunning = true;
      await delay(1);
      resetGraph();
      arrow1to2.style.fill = highlightColor;
      node2.style.fill = highlightColor;
      visualTitle.innerHTML += " " + firstChar + " ";
      node = 1;
      nextNode = 2;
      indexPath += 1;
      openBrackets += 1;

      pathElements.push(arrow1to2);
      pathElements.push(node2);

      stackString += "X";
      stackTitle.innerHTML = "Stack: " + stackString;


    } else {
      isRunning = false;
      node1.style.fill = wrongColor;
      createButton.disabled = false;
      wrongButton.disabled = false;
      playButton.disabled = false;
      return;
    }

    while (isRunning) {

      var delayValue = 0;

      if (exp.length <= 5) {
        delayValue = 1;

      } else if (exp.length <= 10) {
        delayValue = 0.85;

      } else if (exp.length > 10) {
        delayValue = 0.65;
      }

      await delay(delayValue);

      switch (nextNode) {

        case 2:

          if (numbers.includes(exp[indexPath])) {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 2;
            nextNode = 4;
            visualTitle.innerHTML += tempValue;

            resetGraph();
            arrow2to4.style.fill = highlightColor;
            node4.style.fill = highlightColor;

            pathElements.push(arrow2to4);
            pathElements.push(node4);

          } else if (exp[indexPath] === "(") {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 2;
            nextNode = 2;
            openBrackets += 1;
            visualTitle.innerHTML += " " + tempValue + " ";

            resetGraph();
            arrow2to2.style.fill = highlightColor;
            node2.style.fill = highlightColor;

            pathElements.push(arrow2to2);
            pathElements.push(node2);

            stackString += "X";
            stackTitle.innerHTML = "Stack: " + stackString;

          } else {
            resetGraph();
            node2.style.fill = wrongColor;
            isRunning = false;
          }

          break;

        case 3:

        if (numbers.includes(exp[indexPath])) {
          var tempValue = exp[indexPath];
          indexPath += 1;
          node = 3;
          nextNode = 5;
          visualTitle.innerHTML += " " + tempValue + " ";

          resetGraph();
          arrow3to5.style.fill = highlightColor;
          node5.style.fill = highlightColor;

          pathElements.push(arrow3to5);
          pathElements.push(node5);

        } else if (exp[indexPath] === "(") {
          var tempValue = exp[indexPath];
          indexPath += 1;
          node = 3;
          nextNode = 2;
          openBrackets += 1;
          visualTitle.innerHTML += " " + tempValue + " ";

          resetGraph();
          arrow3to2.style.fill = highlightColor;
          node2.style.fill = highlightColor;

          pathElements.push(arrow3to2);
          pathElements.push(node2);

          stackString += "X";
          stackTitle.innerHTML = "Stack: " + stackString;

        } else {
          resetGraph();
          node3.style.fill = wrongColor;
          isRunning = false;
        }

          break;

        case 4:

          if (operators.includes(exp[indexPath])) {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 4;
            nextNode = 3;
            visualTitle.innerHTML += " " + tempValue + " ";

            resetGraph();
            arrow4to3.style.fill = highlightColor;
            node3.style.fill = highlightColor;

            pathElements.push(arrow4to3);
            pathElements.push(node3);

          } else if (exp[indexPath] === ")") {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 4;
            nextNode = 4;
            openBrackets -= 1;
            visualTitle.innerHTML += " " + tempValue + " ";

            resetGraph();
            arrow4to4.style.fill = highlightColor;
            node4.style.fill = highlightColor;

            pathElements.push(arrow4to4);
            pathElements.push(node4);

            var stackCount = stackString.length;
            stackString = stackString.substring(0, stackCount - 1);
            stackTitle.innerHTML = "Stack: " + stackString;


          } else if (numbers.includes(exp[indexPath])) {
            resetGraph();
            node4.style.fill = wrongColor;
            isRunning = false;

          } else if (indexPath === count) {
            if (openBrackets === 0) {
              resetGraph();
              arrow4to6.style.fill = correctColor;
              node6.style.fill = correctColor;

              node = 4;
              nextNode = 6;

              var stackCount = stackString.length;
              stackString = stackString.substring(0, stackCount - 1);
              stackTitle.innerHTML = "Stack: " + stackString;
            }
             else {
               resetGraph();
               node4.style.fill = wrongColor;
               isRunning = false;
            }
          }

          break;

        case 5:

          if (operators.includes(exp[indexPath])) {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 5;
            nextNode = 3;
            visualTitle.innerHTML += " " + tempValue + " ";

            resetGraph();
            arrow5to3.style.fill = highlightColor;
            node3.style.fill = highlightColor;

            pathElements.push(arrow5to3);
            pathElements.push(node3);

          } else if (exp[indexPath] === ")") {
            var tempValue = exp[indexPath];
            indexPath += 1;
            node = 5;
            nextNode = 4;
            openBrackets -= 1;
            visualTitle.innerHTML += " " + tempValue + " ";

            resetGraph();
            arrow5to4.style.fill = highlightColor;
            node4.style.fill = highlightColor;

            pathElements.push(arrow5to4);
            pathElements.push(node4);

            var stackCount = stackString.length;
            stackString = stackString.substring(0, stackCount - 1);
            stackTitle.innerHTML = "Stack: " + stackString;

          } else if (numbers.includes(exp[indexPath])) {
            resetGraph();
            node5.style.fill = wrongColor;
            isRunning = false;

          } else if (exp[indexPath] === "(") {
            resetGraph();
            node5.style.fill = wrongColor;
            isRunning = false;

          } else if (indexPath === count && numbers.includes(exp[count - 1])) {
            if (openBrackets === 0) {
              resetGraph();
              arrow5to6.style.fill = correctColor;
              node6.style.fill = correctColor;

            } else {
              resetGraph();
              node5.style.fill = wrongColor;
              isRunning = false;
            }

            node = 5;
            nextNode = 6;

            var stackCount = stackString.length;
            stackString = stackString.substring(0, stackCount - 1);
            stackTitle.innerHTML = "Stack: " + stackString;
          }

          break;

        case 6:

          isRunning = false;
          break;
    }

  }


    if (nextNode === 6) {
      pathElements.forEach(element => {
        element.style.fill = highlightColor;
      });

    } else {
      pathElements.forEach(element => {
        element.style.fill = highlightColor;
      });

      var elemCount = pathElements.length;
      pathElements[elemCount - 1].style.fill = wrongColor;
      pathElements[elemCount - 2].style.fill = correctColor;
    }


    playButton.disabled = false;
    createButton.disabled = false;
    wrongButton.disabled = false;
}
playButton.addEventListener("click", play);

function startTutorial() {
  introJs().start();
}
tutorialButton.addEventListener("click", startTutorial);

function resetGraph() {
  var nodes = [node0, node1, node2, node3, node4, node5, node6];
  var arrows = [arrow0To1, arrow1to2, arrow1to3, arrow2to2, arrow2to4, arrow3to2, arrow3to5, arrow4to4, arrow4to3, arrow4to6, arrow5to3, arrow5to4, arrow5to6];

  nodes.forEach(node => {
    node.style.fill = "#E1E1E1";
  });

  arrows.forEach(arrow => {
    arrow.style.fill = "#979797";
  });
}

window.onload = function() {
  console.log("Window loaded successfully.");
  playButton.disabled = true;

  selectLengthMenu.value = 5;
  expressionInput.value = "";

  stackTitle.innerHTML = "";
}
